(function ($) {

Drupal.behaviors.indymediaCitiesAccordion = {
  attach: function (context, settings) {
    $('ul.indymedia_cities-accordion', context).once('indymedia-cities-accordion', function () {
      $(this).accordion({ header: "a.indymedia_cities-header", autoHeight: false, collapsible: true, active: false });
    });
  }
};

})(jQuery);
